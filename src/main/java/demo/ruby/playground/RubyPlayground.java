package demo.ruby.playground;

import org.jruby.Ruby;
import org.jruby.RubyObject;
import org.jruby.RubyString;
import org.jruby.embed.jsr223.JRubyEngine;

import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 * @author legkunec
 * @since 01.04.14 17:04
 */
public class RubyPlayground {
    public static void main(String[] args) throws FileNotFoundException, ScriptException, NoSuchMethodException {
        JRubyEngine jruby = (JRubyEngine) new ScriptEngineManager().getEngineByName("jruby");
        Object service = jruby.eval(new FileReader("hello_world.rb"));
        RubyObject rubyService = (RubyObject) service;
        final Object result = rubyService.callMethod("sayHi", RubyString.newString(Ruby.getGlobalRuntime(), "Vlad"));
        System.out.println("result = " + result);

        final Object object = jruby.invokeMethod(service, "sayHi", "ScriptEngine");
        System.out.println("object = " + object);

//        System.out.println("service = " + rubyService.getMetaClass().getMethods());
//        Object javaObject = rubyService.toJava(Object.class);
//        System.out.println("javaObject = " + javaObject);
    }
}
