package demo.ruby;

import org.apache.cxf.service.model.OperationInfo;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author legkunec
 * @since 02.04.14 11:11
 */
public class RubyMethodDispatcher {

    private Map<String, String> methodPerOperationInfo = new ConcurrentHashMap<String, String>();

    void bind(OperationInfo operationInfo, String method) {
        methodPerOperationInfo.put(operationInfo.getName().getLocalPart(), method);
    }

    String getMethod(OperationInfo operationInfo) {
        return methodPerOperationInfo.get(operationInfo.getName().getLocalPart());
    }
}
