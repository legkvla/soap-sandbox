package demo.hw.server;

import java.util.Arrays;

/**
 * @author legkunec
 * @since 23.04.14 10:58
 */
public class TrainContainer {
    private Train[] train;

    public TrainContainer(Train[] trains) {
        this.train = trains;
    }

    public TrainContainer() {
    }

    public Train[] getTrain() {
        return train;
    }

    public void setTrain(Train[] trains) {
        this.train = train;
    }

    @Override
    public String toString() {
        return "TrainContainer{" +
                "trains=" + Arrays.toString(train) +
                '}';
    }
}
