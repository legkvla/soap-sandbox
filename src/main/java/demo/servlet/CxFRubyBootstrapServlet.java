package demo.servlet;

import demo.ruby.SoapHashAegisDataType;
import demo.ruby.WsdlRubyServerFactoryBean;
import org.apache.cxf.Bus;
import org.apache.cxf.BusFactory;
import org.apache.cxf.aegis.databinding.AegisDatabinding;
import org.apache.cxf.aegis.type.DefaultTypeMapping;
import org.apache.cxf.aegis.type.TypeMapping;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.transport.servlet.CXFNonSpringServlet;
import org.jruby.RubyObject;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.servlet.ServletConfig;
import javax.xml.namespace.QName;
import java.io.FileReader;

/**
 * @author legkunec
 * @since 17.04.14 15:22
 */
public class CxFRubyBootstrapServlet extends CXFNonSpringServlet {

    @Override
    public void loadBus(ServletConfig servletConfig) {
        super.loadBus(servletConfig);

        // You could add the endpoint publish codes here
        final Bus bus = BusFactory.getDefaultBus(true);

        bus.getInInterceptors().add(new LoggingInInterceptor());
        bus.getOutInterceptors().add(new LoggingOutInterceptor());

        final ScriptEngine jruby = new ScriptEngineManager().getEngineByName("jruby");
        try {
            final Object service = jruby.eval(new FileReader("hello_world.rb"));

            WsdlRubyServerFactoryBean wsdlServerFactoryBean = new WsdlRubyServerFactoryBean("Hello.wsdl",
                    (RubyObject) service);
            wsdlServerFactoryBean.setBus(bus);

            AegisDatabinding dataBinding = new AegisDatabinding();
            final TypeMapping typeMapping = dataBinding.getAegisContext().getTypeMapping();
            SoapHashAegisDataType type = new SoapHashAegisDataType();
            type.setTypeMapping(typeMapping);
            typeMapping.register(type);

            wsdlServerFactoryBean.setDataBinding(dataBinding);
            wsdlServerFactoryBean.getServiceFactory().setDataBinding(dataBinding);

            QName endpointName = new QName("http://server.hw.demo/", "HelloWorldPort");
            wsdlServerFactoryBean.setEndpointName(endpointName);
            wsdlServerFactoryBean.getServiceFactory().setEndpointName(endpointName);

            wsdlServerFactoryBean.setAddress("/Hello");

            wsdlServerFactoryBean.create();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
