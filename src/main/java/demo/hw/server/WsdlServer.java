/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package demo.hw.server;

import org.apache.cxf.Bus;
import org.apache.cxf.aegis.databinding.AegisDatabinding;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;

import javax.xml.namespace.QName;

public final class WsdlServer {
    private WsdlServer() {
    }

    public static void startServer() throws Exception {
        HelloWorldOld helloWorldService = new HelloWorldOld();
        WsdlServerFactoryBean wsdlServerFactoryBean = new WsdlServerFactoryBean("Hello.wsdl", helloWorldService);
        AegisDatabinding dataBinding = new AegisDatabinding();
        wsdlServerFactoryBean.setDataBinding(dataBinding);
        wsdlServerFactoryBean.getServiceFactory().setDataBinding(dataBinding);

        QName endpointName = new QName("http://server.hw.demo/", "HelloWorldPort");
        wsdlServerFactoryBean.setEndpointName(endpointName);
        wsdlServerFactoryBean.getServiceFactory().setEndpointName(endpointName);

        Bus bus = wsdlServerFactoryBean.getServiceFactory().getBus();
        bus.getInInterceptors().add(new LoggingInInterceptor());
        bus.getOutInterceptors().add(new LoggingOutInterceptor());

        wsdlServerFactoryBean.create();

    }

    public static void main(String args[]) throws Exception {
        startServer();
        System.out.println("Server ready...");

        Thread.sleep(5 * 60 * 1000);
        System.out.println("Server exiting");
        System.exit(0);
    }
}
