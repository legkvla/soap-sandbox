package demo.ruby;

import org.apache.cxf.BusException;
import org.apache.cxf.common.classloader.ClassLoaderUtils;
import org.apache.cxf.endpoint.Endpoint;
import org.apache.cxf.endpoint.EndpointException;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.endpoint.ServerImpl;
import org.apache.cxf.feature.Feature;
import org.apache.cxf.service.factory.ServiceConstructionException;
import org.apache.cxf.service.invoker.Invoker;
import org.jruby.RubyObject;

import java.io.IOException;

/**
 * This class code got from ServerFactoryBean
 */
public class WsdlRubyServerFactoryBean extends AbstractWsdlServiceFactoryForRuby {

    private Server server;
    private Invoker invoker;

    private final RubyObject serviceBean;

    public WsdlRubyServerFactoryBean(String wsdlUrl, RubyObject serviceBean) {
        super(wsdlUrl, serviceBean);
        this.serviceBean = serviceBean;
    }

    public Server create() {
        ClassLoaderUtils.ClassLoaderHolder orig = null;
        try {
            try {
                if (bus != null) {
                    ClassLoader loader = bus.getExtension(ClassLoader.class);
                    if (loader != null) {
                        orig = ClassLoaderUtils.setThreadContextClassloader(loader);
                    }
                }

                invoker = createInvoker();

                Endpoint ep = createEndpoint();
                server = new ServerImpl(getBus(),
                        ep,
                        getDestinationFactory(),
                        getBindingFactory());

                if (ep.getService().getInvoker() == null) {
                    ep.getService().setInvoker(invoker);
                }
                serviceFactory.initInfrastructure();

            } catch (EndpointException e) {
                throw new ServiceConstructionException(e);
            } catch (BusException e) {
                throw new ServiceConstructionException(e);
            } catch (IOException e) {
                throw new ServiceConstructionException(e);
            }

            applyFeatures();

            try {
                server.start();
            } catch (RuntimeException re) {
                server.destroy(); // prevent resource leak
                throw re;
            }
            return server;
        } finally {
            if (orig != null) {
                orig.reset();
            }
        }
    }

    public Invoker getInvoker() {
        return invoker;
    }

    public void setInvoker(Invoker invoker) {
        this.invoker = invoker;
    }

    public RubyObject getServiceBean() {
        return serviceBean;
    }

    protected Invoker createInvoker() {
        return new RubyInvoker(getServiceBean());
    }

    protected void applyFeatures() {
        if (getFeatures() != null) {
            for (Feature feature : getFeatures()) {
                feature.initialize(server, getBus());
            }
        }
    }
}
