package demo.ruby;

import org.apache.cxf.aegis.Context;
import org.apache.cxf.aegis.DatabindingException;
import org.apache.cxf.aegis.type.AegisType;
import org.apache.cxf.aegis.xml.MessageReader;
import org.apache.cxf.aegis.xml.MessageWriter;

import javax.xml.namespace.QName;

/**
 * @author legkunec
 * @since 22.04.14 17:30
 */
public class SoapHashAegisDataType extends AegisType{

    public SoapHashAegisDataType() {
        typeClass = SoapHash.class;
        //todo little hack
        setSchemaType(new QName("", ""));

    }

    @Override
    public Object readObject(MessageReader reader, Context context) throws DatabindingException {
        final SoapHash soapHash = new SoapHash(reader.getLocalName());
        while (reader.hasMoreElementReaders()) {
            MessageReader childReader = reader.getNextElementReader();
            if (childReader.hasMoreElementReaders()){
                soapHash.addChild(parseSoapHash(childReader));
            } /*else {
                QName name = childReader.getName();
                soapHash.addAttribute(name.getLocalPart(), childReader.getValue());
            }*/
        }
        return soapHash;
    }

    //we need to have parse in this method because we can lost position into stream
    public SoapHash parseSoapHash(MessageReader reader){
        final SoapHash soapHash = new SoapHash(reader.getLocalName());
        while (reader.hasMoreElementReaders()){
            MessageReader childReader = reader.getNextElementReader();
            soapHash.addAttribute(childReader.getLocalName(), childReader.getValue());
        }
        return soapHash;
    }

    @Override
    public void writeObject(Object object, MessageWriter writer, Context context) throws DatabindingException {
        //todo implement in the same maner
    }
}
