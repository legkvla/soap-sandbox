package demo.hw.server;

/**
 * @author legkunec
 * @since 22.04.14 16:18
 */
public class Train {
    private int id;
    private String name;

    public Train(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Train() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Train{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
