package demo.hw.server;

import org.apache.cxf.BusException;
import org.apache.cxf.aegis.databinding.AegisDatabinding;
import org.apache.cxf.binding.BindingFactory;
import org.apache.cxf.binding.BindingFactoryManager;
import org.apache.cxf.binding.soap.SoapBindingConfiguration;
import org.apache.cxf.binding.soap.SoapBindingFactory;
import org.apache.cxf.common.i18n.Message;
import org.apache.cxf.common.logging.LogUtils;
import org.apache.cxf.endpoint.AbstractEndpointFactory;
import org.apache.cxf.endpoint.Endpoint;
import org.apache.cxf.endpoint.EndpointException;
import org.apache.cxf.endpoint.EndpointImpl;
import org.apache.cxf.frontend.WSDLGetInterceptor;
import org.apache.cxf.service.Service;
import org.apache.cxf.service.factory.FactoryBeanListener;
import org.apache.cxf.service.factory.ServiceConstructionException;
import org.apache.cxf.service.model.BindingInfo;
import org.apache.cxf.service.model.EndpointInfo;
import org.apache.cxf.wsdl11.WSDLServiceFactory;

import java.util.logging.Logger;

/**
 * @author legkunec
 * @since 28.03.14 17:27
 */
public abstract class AbstractWsdlServiceFactory extends AbstractEndpointFactory {
    private static final Logger LOG = LogUtils.getL7dLogger(AbstractWsdlServiceFactory.class);

    protected final WSDLServiceFactoryForServer serviceFactory;
    private Service service;

    public AbstractWsdlServiceFactory(String wsdlUrl, Class serviceClass) {
        serviceFactory = new WSDLServiceFactoryForServer(getBus(), wsdlUrl, serviceClass);
        serviceFactory.setDataBinding(new AegisDatabinding());
    }

    @Override
    public BindingFactory getBindingFactory() {
        return new SoapBindingFactory(getBus());
    }

    @Override
    protected Endpoint createEndpoint() throws BusException, EndpointException {
        EndpointInfo ei = getService().getEndpointInfo(getEndpointName());

        BindingFactoryManager bfm = getBus().getExtension(BindingFactoryManager.class);
        bindingFactory = bfm.getBindingFactory(ei.getBinding().getBindingId());

        Endpoint ep = new EndpointImpl(getBus(), getService(), ei);

        service.getEndpoints().put(ep.getEndpointInfo().getName(), ep);

        if (getInInterceptors() != null) {
            ep.getInInterceptors().addAll(getInInterceptors());
            ep.getInInterceptors().add(WSDLGetInterceptor.INSTANCE);
        }
        if (getOutInterceptors() != null) {
            ep.getOutInterceptors().addAll(getOutInterceptors());
        }
        if (getInFaultInterceptors() != null) {
            ep.getInFaultInterceptors().addAll(getInFaultInterceptors());
        }
        if (getOutFaultInterceptors() != null) {
            ep.getOutFaultInterceptors().addAll(getOutFaultInterceptors());
        }

        //sendEvent(FactoryBeanListener.Event.ENDPOINT_SELECTED, ei, ep);
        return ep;
    }

    public WSDLServiceFactory getServiceFactory() {
        return serviceFactory;
    }

    protected SoapBindingConfiguration createSoapBindingConfig() {
        return new SoapBindingConfiguration();
    }

    /**
     * This method got from AbstractWSDLBasedEndpointFactory
     */
    @Override
    protected BindingInfo createBindingInfo() {
        BindingFactoryManager mgr = bus.getExtension(BindingFactoryManager.class);
        String binding = bindingId;

        if (binding == null && bindingConfig != null) {
            binding = bindingConfig.getBindingId();
        }

        if (binding == null) {
            // default to soap binding
            binding = "http://schemas.xmlsoap.org/soap/";
        }

        try {
            if (binding.contains("/soap")) {
                if (bindingConfig == null) {
                    bindingConfig = createSoapBindingConfig();
                }
                //todo may be we should comment this code???
                if (bindingConfig instanceof SoapBindingConfiguration
                        && !((SoapBindingConfiguration) bindingConfig).isSetStyle()) {
                    ((SoapBindingConfiguration) bindingConfig).setStyle("document");
                }
            }

            bindingFactory = mgr.getBindingFactory(binding);

            BindingInfo inf = bindingFactory.createBindingInfo(serviceFactory.getService(),
                    binding, bindingConfig);

            /*for (BindingOperationInfo boi : inf.getOperations()) {
                serviceFactory.updateBindingOperation(boi);
                Method m = serviceFactory.getMethodDispatcher().getMethod(boi);
                serviceFactory.sendEvent(FactoryBeanListener.Event.BINDING_OPERATION_CREATED, inf, boi, m);
            }*/
            serviceFactory.sendEvent(FactoryBeanListener.Event.BINDING_CREATED, inf);
            return inf;
        } catch (BusException ex) {
            throw new ServiceConstructionException(
                    new Message("COULD.NOT.RESOLVE.BINDING", LOG, bindingId), ex);
        }
    }

    public Service getService() {
        if (service == null) {
            service = serviceFactory.create();
            service.setDataBinding(dataBinding);
        }
        return service;
    }

    protected void initInfrastructure() {
        serviceFactory.initInfrastructure();
    }
}
