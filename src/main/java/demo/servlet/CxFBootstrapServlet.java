package demo.servlet;

import org.apache.cxf.Bus;
import org.apache.cxf.BusFactory;
import org.apache.cxf.frontend.ServerFactoryBean;
import org.apache.cxf.transport.servlet.CXFNonSpringServlet;

import javax.servlet.ServletConfig;

/**
 * @author legkunec
 * @since 17.04.14 15:22
 */
public class CxFBootstrapServlet extends CXFNonSpringServlet {

    @Override
    public void loadBus(ServletConfig servletConfig) {
        super.loadBus(servletConfig);

        // You could add the endpoint publish codes here
        Bus bus = BusFactory.getDefaultBus(true);

        // You can als use the simple frontend API to do this
        ServerFactoryBean factory = new ServerFactoryBean();
        factory.setBus(bus);
        factory.setServiceClass(GreeterImpl.class);
        factory.setAddress("/Greeter");
        factory.create();
    }

}
