package demo.ruby;

import org.apache.cxf.Bus;
import org.apache.cxf.aegis.type.AegisType;
import org.apache.cxf.aegis.type.DefaultTypeMapping;
import org.apache.cxf.common.logging.LogUtils;
import org.apache.cxf.service.factory.FactoryBeanListener;
import org.apache.cxf.service.model.EndpointInfo;
import org.apache.cxf.service.model.InterfaceInfo;
import org.apache.cxf.service.model.MessagePartInfo;
import org.apache.cxf.service.model.OperationInfo;
import org.apache.cxf.wsdl11.WSDLServiceFactory;
import org.jruby.RubyObject;

import javax.xml.namespace.QName;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author legkunec
 * @since 31.03.14 15:32
 */
public class WSDLServiceFactoryForRuby extends WSDLServiceFactory {

    private static final Logger LOG = LogUtils.getL7dLogger(WSDLServiceFactoryForRuby.class);

    private final RubyMethodDispatcher methodDispatcher = new RubyMethodDispatcher();

    private final RubyObject rubyObject;

    private QName endpointName;

    private DefaultTypeMapping typeMapping = DefaultTypeMapping.createSoap11TypeMapping(true, false, true);

    public WSDLServiceFactoryForRuby(Bus b, String url, RubyObject rubyObject) {
        super(b, url);
        this.rubyObject = rubyObject;
    }

    public RubyMethodDispatcher getMethodDispatcher() {
        return methodDispatcher;
    }

    public void initInfrastructure() {
        getService().put(RubyMethodDispatcher.class.getName(), methodDispatcher);

        initializeDefaultInterceptors();
        initializeWSDLOperations();
        initializeDataBindings();
    }

    protected void initializeWSDLOperations() {

        Set<String> methods = rubyObject.getMetaClass().getMethods().keySet();

        InterfaceInfo intf = getInterfaceInfo();

        Map<QName, String> validMethods = new HashMap<QName, String>();
        for (String m : methods) {
            QName opName = getOperationName(intf, m);
            if (opName != null) {
                validMethods.put(opName, m);
            }
        }

        for (OperationInfo o : intf.getOperations()) {
            String selected = null;
            for (Map.Entry<QName, String> m : validMethods.entrySet()) {
                QName opName = m.getKey();

                if (o.getName().getNamespaceURI().equals(opName.getNamespaceURI())
                        && o.getName().getLocalPart().equals(opName.getLocalPart())) {
                    selected = m.getValue();
                    break;
                }
            }

            if (selected == null) {
                LOG.log(Level.WARNING, "NO_METHOD_FOR_OP", o.getName());
            } else {
                initializeWSDLOperation(o, selected);
            }
        }
        sendEvent(FactoryBeanListener.Event.INTERFACE_CREATED, intf, rubyObject.getJavaClass());
    }

    protected void initializeWSDLOperation(OperationInfo o, String method) {
        populateTypeClasses(o.getUnwrappedOperation().getInput().getMessageParts());
        populateTypeClasses(o.getUnwrappedOperation().getOutput().getMessageParts());

        bindOperation(o, method);
    }

    private void populateTypeClasses(List<MessagePartInfo> messageParts) {
        for (MessagePartInfo messagePart : messageParts) {
            AegisType type = typeMapping.getType(messagePart.getTypeQName());
            Class<?> typeClass = type == null ? SoapHash.class : type.getTypeClass();
            messagePart.setTypeClass(typeClass);
        }
    }

    protected void bindOperation(OperationInfo op, String m) {
        getMethodDispatcher().bind(op, m);
    }

    public InterfaceInfo getInterfaceInfo() {
        return getEndpointInfo().getInterface();
    }

    private EndpointInfo getEndpointInfo() {
        return getService().getEndpointInfo(endpointName);
    }

    @Override
    public void setEndpointName(QName qn) {
        super.setEndpointName(qn);
        this.endpointName = qn;
    }

    /**
     * Creates a name for the operation from the method name. If an operation
     * with that name already exists, a name is create by appending an integer
     * to the end. I.e. if there is already two methods named
     * <code>doSomething</code>, the first one will have an operation name of
     * "doSomething" and the second "doSomething1".
     */
    protected QName getOperationName(InterfaceInfo service, String method) {
        Collection<OperationInfo> operations = service.getOperations();
        for (OperationInfo operation : operations) {
            if (method.equals(operation.getName().getLocalPart())) return operation.getName();
        }
        return null;
    }
}
