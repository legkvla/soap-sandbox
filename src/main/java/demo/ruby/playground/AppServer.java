package demo.ruby.playground;

import org.jruby.Ruby;
import org.jruby.RubyObject;
import org.jruby.javasupport.JavaUtil;
import org.jruby.runtime.builtin.IRubyObject;

import java.util.List;
import java.util.Map;

/**
 * @author legkunec
 * @since 01.04.14 18:58
 */
public class AppServer {

    private final RubyObject component;

    public AppServer(RubyObject component) {
        this.component = component;
    }


    public String evalComponent() {
        final String[] parameters = new String[]{"component"};
        IRubyObject result = component.callMethod("sayHi",
                JavaUtil.convertJavaArrayToRuby(Ruby.getGlobalRuntime(), parameters));
        System.out.println("result.getJavaClass() = " + result.getJavaClass());
        if (result.getJavaClass().equals(Map.class)) {
            Map map = (Map) result.toJava(Map.class);
            //ruby symbols returned
            return (String) map.get("hello");
        }
        if (result.getJavaClass().equals(List.class)) {
            List list = (List) result.toJava(List.class);
            return (String) list.get(1);
        } else {
            return result.toString();
        }
    }


    @Override
    public String toString() {
        return "AppServer{" +
                "component=" + component +
                '}';
    }
}
