require 'java'

java_import 'demo.ruby.playground.AppServer'

class Service

  def sayHi(message)
    #["Hello: #{message}", "Bye: #{message}"]
    {:hello => "Hello: #{message}", :bye => "Bye: #{message}"}
  end

end

service = Service.new

app_server = AppServer.new(service)

puts app_server.evalComponent


