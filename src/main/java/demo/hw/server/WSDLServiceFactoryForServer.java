package demo.hw.server;

import org.apache.cxf.Bus;
import org.apache.cxf.common.logging.LogUtils;
import org.apache.cxf.service.factory.FactoryBeanListener;
import org.apache.cxf.service.factory.SimpleMethodDispatcher;
import org.apache.cxf.service.invoker.MethodDispatcher;
import org.apache.cxf.service.model.EndpointInfo;
import org.apache.cxf.service.model.InterfaceInfo;
import org.apache.cxf.service.model.OperationInfo;
import org.apache.cxf.wsdl11.WSDLServiceFactory;

import javax.xml.namespace.QName;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author legkunec
 * @since 31.03.14 15:32
 */
public class WSDLServiceFactoryForServer extends WSDLServiceFactory {

    private static final Logger LOG = LogUtils.getL7dLogger(WSDLServiceFactoryForServer.class);

    private final MethodDispatcher methodDispatcher = new SimpleMethodDispatcher();

    private final Class serviceClass;

    private QName endpointName;

    public WSDLServiceFactoryForServer(Bus b, String url, Class serviceClass) {
        super(b, url);
        this.serviceClass = serviceClass;
    }

    public MethodDispatcher getMethodDispatcher() {
        return methodDispatcher;
    }

    public void initInfrastructure() {
        getService().put(MethodDispatcher.class.getName(), methodDispatcher);
        initializeDataBindings();
        initializeDefaultInterceptors();
        initializeWSDLOperations();
    }

    protected void initializeWSDLOperations() {
        Method[] methods = serviceClass.getMethods();

        InterfaceInfo intf = getInterfaceInfo();

        Map<QName, Method> validMethods = new HashMap<QName, Method>();
        for (Method m : methods) {
            QName opName = getOperationName(intf, m);
            if (opName != null) {
                validMethods.put(opName, m);
            }
        }

        for (OperationInfo o : intf.getOperations()) {
            Method selected = null;
            for (Map.Entry<QName, Method> m : validMethods.entrySet()) {
                QName opName = m.getKey();

                if (o.getName().getNamespaceURI().equals(opName.getNamespaceURI())
                        && o.getName().getLocalPart().equals(opName.getLocalPart())) {
                    selected = m.getValue();
                    break;
                }
            }

            if (selected == null) {
                LOG.log(Level.WARNING, "NO_METHOD_FOR_OP", o.getName());
            } else {
                initializeWSDLOperation(o, selected);
            }
        }
        sendEvent(FactoryBeanListener.Event.INTERFACE_CREATED, intf, serviceClass);
    }

    private void initializeWSDLOperation(OperationInfo o, Method method) {
        //todo we need alternative initialize class info to fill in from wsdl
        bindOperation(o, method);
        sendEvent(FactoryBeanListener.Event.INTERFACE_OPERATION_BOUND, o, method);
    }

    private void bindOperation(OperationInfo op, Method m) {
        getMethodDispatcher().bind(op, m);
    }

    protected InterfaceInfo getInterfaceInfo() {
        return getEndpointInfo().getInterface();
    }

    protected EndpointInfo getEndpointInfo() {
        return getService().getEndpointInfo(endpointName);
    }

    @Override
    public void setEndpointName(QName qn) {
        super.setEndpointName(qn);
        this.endpointName = qn;
    }

    /**
     * Creates a name for the operation from the method name. If an operation
     * with that name already exists, a name is create by appending an integer
     * to the end. I.e. if there is already two methods named
     * <code>doSomething</code>, the first one will have an operation name of
     * "doSomething" and the second "doSomething1".
     */
    protected QName getOperationName(InterfaceInfo service, Method method) {
        Collection<OperationInfo> operations = service.getOperations();
        for (OperationInfo operation : operations) {
            if (method.getName().equals(operation.getName().getLocalPart())) return operation.getName();
        }
        return null;
    }
}
