package demo.servlet;

/**
 * @author legkunec
 * @since 17.04.14 15:28
 */
public class GreeterImpl {

    public String echo(String echoParam) {
        System.out.println("[GREETER] echoParam = " + echoParam);
        return echoParam;
    }

}
