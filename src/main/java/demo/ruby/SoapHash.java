package demo.ruby;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author legkunec
 * @since 22.04.14 16:46
 */
public class SoapHash {

    private final String componentName;
    private final Map<String, String> attrs = new HashMap<String, String>();
    private final List<SoapHash> children = new ArrayList<SoapHash>();

    public SoapHash(String componentName) {
        this.componentName = componentName;
    }

    public String getComponentName() {
        return componentName;
    }

    public void addChild(SoapHash soapHash){
        children.add(soapHash);
    }

    public void addAttribute(String name, String value){
        attrs.put(name, value);
    }

    public String getAttribute(String name){
        return attrs.get(name);
    }

    @Override
    public String toString() {
        if (children.size() > 0) {
            return componentName + " : " + children.toString();
        } else {
            return componentName + " : " + attrs.toString();
        }
    }
}
