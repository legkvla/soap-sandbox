package demo.hw.client;

import demo.hw.server.Train;
import demo.hw.server.TrainContainer;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.endpoint.dynamic.DynamicClientFactory;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author legkunec
 * @since 28.03.14 16:43
 */
public class DynamicClient {
    public static void main(String[] args) throws Exception {
        callHello();
    }

    private static void callGreeter() throws Exception {
        DynamicClientFactory dcf = DynamicClientFactory.newInstance();
        Client client = dcf.createClient("Greeter.wsdl");
//        client.getBus().getFeatures().add(new LoggingFeature());
        client.getBus().getInInterceptors().add(new LoggingInInterceptor());
        client.getBus().getOutInterceptors().add(new LoggingOutInterceptor());
        Object[] results = client.invoke("echo", "Dynamic Vladimir");
        for (Object result : results) {
            System.out.println("result = " + result);
        }
    }

    private static void callHello() throws Exception {
        DynamicClientFactory dcf = DynamicClientFactory.newInstance();
        Client client = dcf.createClient("Hello.wsdl");
//        client.getBus().getFeatures().add(new LoggingFeature());
        client.getBus().getInInterceptors().add(new LoggingInInterceptor());
        client.getBus().getOutInterceptors().add(new LoggingOutInterceptor());
//        ArrayList list = new ArrayList();
//        list.add("Dynamic Vladimir");
        Object[] results = client.invoke("sayHi",
                new TrainContainer(new Train[]{
                        new Train(1, "Strela"), new Train(2, "Sapsan")
                })
        );
        for (Object result : results) {
            System.out.println("result = " + result);
        }
    }

    private static void callHelloWorld() throws Exception {
        DynamicClientFactory dcf = DynamicClientFactory.newInstance();
        Client client = dcf.createClient("HelloWorld.wsdl");
//        client.getBus().getFeatures().add(new LoggingFeature());
        client.getBus().getInInterceptors().add(new LoggingInInterceptor());
        client.getBus().getOutInterceptors().add(new LoggingOutInterceptor());
        Object[] results = client.invoke("sayHi", "Dynamic Vladimir");
        for (Object result : results) {
            System.out.println("result = " + result);
        }
    }
}
