package demo.ruby;

import org.apache.cxf.common.i18n.Message;
import org.apache.cxf.common.logging.LogUtils;
import org.apache.cxf.continuations.SuspendedInvocationException;
import org.apache.cxf.helpers.CastUtils;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Exchange;
import org.apache.cxf.message.FaultMode;
import org.apache.cxf.message.MessageContentsList;
import org.apache.cxf.service.Service;
import org.apache.cxf.service.invoker.Invoker;
import org.apache.cxf.service.model.BindingOperationInfo;
import org.jruby.Ruby;
import org.jruby.RubyObject;
import org.jruby.javasupport.JavaUtil;
import org.jruby.runtime.builtin.IRubyObject;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author legkunec
 * @since 02.04.14 11:51
 */
public class RubyInvoker implements Invoker {

    private static final Logger LOG = LogUtils.getL7dLogger(RubyInvoker.class);

    private final RubyObject serviceObject;

    public RubyInvoker(RubyObject rubyObject) {
        serviceObject = rubyObject;
    }

    @Override
    public Object invoke(Exchange exchange, Object o) {
        BindingOperationInfo bop = exchange.get(BindingOperationInfo.class);
        RubyMethodDispatcher md = (RubyMethodDispatcher)
                exchange.get(Service.class).get(RubyMethodDispatcher.class.getName());

        String m = bop == null ? null : md.getMethod(bop.getOperationInfo());
        if (m == null && bop == null) {
            LOG.severe(new Message("MISSING_BINDING_OPERATION", LOG).toString());
            throw new Fault(new Message("EXCEPTION_INVOKING_OBJECT", LOG,
                    "No binding operation info", "unknown method", "unknown"));
        }
        List<Object> params = null;
        if (o instanceof List) {
            params = CastUtils.cast((List<?>) o);
        } else if (o != null) {
            params = new MessageContentsList(o);
        }

        return invoke(exchange, getServiceObject(), m, params);
    }

    public RubyObject getServiceObject() {
        return serviceObject;
    }

    protected Object invoke(Exchange exchange, final RubyObject serviceObject, String method, List<Object> params) {
        Object res;
        try {
            Object[] paramArray = new Object[]{};
            if (params != null) {
                paramArray = params.toArray();
            }

            res = performInvocation(serviceObject, method, paramArray);

            if (exchange.isOneWay()) {
                return null;
            }

            return new MessageContentsList(res);
        } catch (InvocationTargetException e) {

            Throwable t = e.getCause();

            if (t == null) {
                t = e;
            }

            checkSuspendedInvocation(exchange, serviceObject, method, params, t);

            exchange.getInMessage().put(FaultMode.class, FaultMode.UNCHECKED_APPLICATION_FAULT);


//            for (Class<?> cl : method.getExceptionTypes()) {
//                if (cl.isInstance(t)) {
//                    exchange.getInMessage().put(FaultMode.class,
//                            FaultMode.CHECKED_APPLICATION_FAULT);
//                }
//            }

            if (t instanceof Fault) {
                exchange.getInMessage().put(FaultMode.class,
                        FaultMode.CHECKED_APPLICATION_FAULT);
                throw (Fault) t;
            }
            throw createFault(t, method, params, true);
        } catch (SuspendedInvocationException suspendedEx) {
            // to avoid duplicating the same log statement
            checkSuspendedInvocation(exchange, serviceObject, method, params, suspendedEx);
            // unreachable
            throw suspendedEx;
        } catch (Fault f) {
            exchange.getInMessage().put(FaultMode.class, FaultMode.UNCHECKED_APPLICATION_FAULT);
            throw f;
        } catch (Exception e) {
            checkSuspendedInvocation(exchange, serviceObject, method, params, e);
            exchange.getInMessage().put(FaultMode.class, FaultMode.UNCHECKED_APPLICATION_FAULT);
            throw createFault(e, method, params, false);
        }
    }

    private Object performInvocation(RubyObject serviceObject, String method, Object[] paramArray) throws Exception {
        IRubyObject rubyObject = serviceObject.callMethod(method, JavaUtil.convertJavaArrayToRuby(Ruby.getGlobalRuntime(), paramArray));
        //todo lists are fine but possibly we will need special code to convert hash that used symbols. There are some options how to deal with it.
        return rubyObject.toJava(rubyObject.getJavaClass());
    }

    protected void checkSuspendedInvocation(Exchange exchange,
                                            Object serviceObject,
                                            String m,
                                            List<Object> params,
                                            Throwable t) {
        if (t instanceof SuspendedInvocationException) {

            if (LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, "SUSPENDED_INVOCATION_EXCEPTION",
                        new Object[]{serviceObject, m, params});
            }
            throw (SuspendedInvocationException) t;
        }
    }

    protected Fault createFault(Throwable ex, String m, List<Object> params, boolean checked) {

        if (checked) {
            return new Fault(ex);
        } else {
            String message = (ex == null) ? "" : ex.getMessage();
            String method = (m == null) ? "<null>" : m;
            return new Fault(new Message("EXCEPTION_INVOKING_OBJECT", LOG,
                    message, method, params),
                    ex);
        }
    }
}
