package demo.servlet;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ServletInJetty {
    public static void main(String[] args) throws Exception {
        Server server = new Server(8090);
        HandlerCollection handlerList = new HandlerCollection();
        server.setHandler(handlerList);
        ServletContextHandler context = new ServletContextHandler(handlerList, "/", ServletContextHandler.SESSIONS | ServletContextHandler.SECURITY);

        context.addServlet(new ServletHolder(new DefaultServlet() {
            @Override
            protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                response.getWriter().append("{\"message\":\"Hello, friend!\"}");
                //append(request.getUserPrincipal().getName());
            }

            @Override
            protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                doGet(request, response);
            }
        }), "/hello");
        context.addServlet(new ServletHolder(new CxFRubyBootstrapServlet()), "/services/*");

        server.start();
        server.join();
    }
}
